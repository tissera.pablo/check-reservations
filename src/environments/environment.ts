// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDJqHyc59sdfxixFzVeniJ_N4gfnabM5G4",
    authDomain: "check-reservations.firebaseapp.com",
    databaseURL: "https://check-reservations.firebaseio.com",
    projectId: "check-reservations",
    storageBucket: "check-reservations.appspot.com",
    messagingSenderId: "987625891275",
    appId: "1:987625891275:web:7104c1899138cc7eafc24e",
    measurementId: "G-1MY22CM9R0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
