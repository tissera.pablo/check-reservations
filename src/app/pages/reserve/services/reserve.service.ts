import { Injectable } from '@angular/core';
import { Reserve } from '../models/reserve.model';
import { FirestoreService } from '../../../core/services/firestore.service';

@Injectable({
  providedIn: 'root',
})
export class ReserveService extends FirestoreService<Reserve>{
  protected basePath: string = 'reserves';
}
