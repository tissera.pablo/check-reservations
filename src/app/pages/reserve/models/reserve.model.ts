export class Reserve {
  id: string;
  startDate: string;
  totalDays: number;
  totalPerson: number;
  dptoId: number;
  reservationMethod: number;
  paymentMethod: number;
  pricePerDay: number;
  observations: string;
  status: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}
