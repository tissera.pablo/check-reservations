import { Component, OnInit, OnDestroy } from '@angular/core';
import { ReserveService } from '../../services/reserve.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import { paymentMethodsDesc } from '../../constants/reserve.constant';

@Component({
  selector: 'app-reserve-list',
  templateUrl: './reserve-list.component.html',
  styleUrls: ['./reserve-list.component.css']
})
export class ReserveListComponent implements OnInit, OnDestroy {

  private listSubscription: Subscription;
  public list: any = [];

  constructor(private reserveService: ReserveService, private router: Router) {
  }

  ngOnDestroy(): void {
    this.listSubscription.unsubscribe();
  }

  ngOnInit() {
    this.listSubscription = this.reserveService.collection$().subscribe(
      res => {
        _.forEach(res, (item: any) => {
          item.startDate = new Date(item.startDate).toISOString().substring(0, 10);
          item.paymentMethodDescription = paymentMethodsDesc[item.paymentMethod].name;
        });
        this.list = res;
      },
      err => {
        console.log(err);
      }
    );
  }

  delete(id: string) {
    this.reserveService.delete(id).then(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }

}
