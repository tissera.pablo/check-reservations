import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReserveFormComponent } from './components/reserve-form/reserve-form.component';
import { ReserveListComponent } from './components/reserve-list/reserve-list.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { UiCoreModule } from 'src/app/ui-core/ui-core.module';
import { AuthGuard } from '../../core/guard/auth.guard';
import { MaterialModule } from '../../material-module';

const routes: Routes = [
  {
    path: 'reserve',
    component: ReserveListComponent,
    canActivate: [AuthGuard]
  },
    {
    path: 'reserve/add',
    component: ReserveFormComponent,
    canActivate: [AuthGuard]
    },
    {
    path: 'reserve/edit/:id',
    component: ReserveFormComponent,
    canActivate: [AuthGuard]
    }
];

@NgModule({
  declarations: [
    ReserveFormComponent, 
    ReserveListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    ReactiveFormsModule,
    UiCoreModule,
    MaterialModule
  ],
  exports: [
    ReserveFormComponent
  ]
})
export class ReserveModule { }
