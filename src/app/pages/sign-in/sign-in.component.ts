import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../core/services/auth.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})

export class SignInComponent implements OnInit {

  signinForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private fb: FormBuilder,
    public authService: AuthService,
    private router:Router,
    private activedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.signinForm = this.fb.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
    });

    // reset login status
    this.authService.SignOut();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.activedRoute.snapshot.queryParams['returnUrl'] || '/';
}

// convenience getter for easy access to form fields
get f() { return this.signinForm.controls; }

onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.signinForm.invalid) {
        return;
    }

    this.loading = true;
    this.authService.SignIn(this.signinForm.value);
}

}