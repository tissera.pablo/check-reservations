import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ReserveService } from '../../services/reserve.service';
import { dptos, paymentMethods, reservationFrom } from '../../constants/reserve.constant';
import { GenericItem } from '../../../../core/models/generic-item-model';
import { RouterUtilsService } from '../../../../core/services/router-utils.service';

@Component({
  selector: 'app-reserve-form',
  templateUrl: './reserve-form.component.html',
  styleUrls: ['./reserve-form.component.css']
})
export class ReserveFormComponent implements OnInit {
  thisForm = this.fb.group({
    startDate: [new Date().toISOString().substring(0, 10), Validators.required],
    totalDays: [2, Validators.required],
    totalPerson: [1, Validators.required],
    dptoId: [1, Validators.required],
    reservationMethod: [1, Validators.required],
    paymentMethod: [1, Validators.required],
    pricePerDay: [0, Validators.required],
    observations: [''],
    status: [1],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: [''],
    phone: ['']
  });

  id = '0';
  dptos: GenericItem[];
  paymentMethods: GenericItem[];
  reservationFrom: GenericItem[];
  step = 0;

  constructor(private fb: FormBuilder, private reserveService: ReserveService,
              private router: Router, private activedRoute: ActivatedRoute,
              private routerService: RouterUtilsService) {
      this.dptos = dptos;
      this.paymentMethods = paymentMethods;
      this.reservationFrom = reservationFrom;
    }

  ngOnInit() {
    const {id} = this.activedRoute.snapshot.params;
    if (id) {
      this.id = id;
      this.reserveService.doc$(this.id).subscribe(
        res => {
          res.startDate = new Date(res.startDate).toISOString().substring(0, 10);
          this.thisForm.patchValue(res);
        },
        err => console.log(err)
      );
    }
  }

  onSubmit() {
  let value = this.thisForm.value;
  if (value.startDate["getDate"] != null) {
    value.startDate = value.startDate.toISOString().substring(0, 10);
  }
    const save: Promise<any> = (this.id === '0') ?
      this.reserveService.create(value) :
      this.reserveService.update(this.id, value);

    save.then(
      res => {
        this.onClose(true);
      },
      err => console.log(err)
    );
  }

  onClose(update: boolean= false) {
    this.router.navigate([this.routerService.getPreviousUrl()]);
  }

    setStep(index: number) {
      this.step = index;
    }

    nextStep() {
      this.step++;
    }

    prevStep() {
      this.step--;
    }
}
