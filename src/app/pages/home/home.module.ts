import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { UiCoreModule } from 'src/app/ui-core/ui-core.module';
import { HomeComponent } from './home.component';
import { ReserveFormComponent } from '../reserve/components/reserve-form/reserve-form.component';
import { AuthGuard } from '../../core/guard/auth.guard';
import { MaterialModule } from '../../material-module';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    ReactiveFormsModule,
    UiCoreModule,
    MaterialModule
  ]
})
export class HomeModule { }
