import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReserveModule } from './reserve/reserve.module';
import { UiCoreModule } from '../ui-core/ui-core.module';
import { HomeModule } from './home/home.module';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SecureInnerPagesGuard } from '../core/guard/secure-inner-pages.guard';
import { Routes, RouterModule } from '@angular/router';
import { FirestoreService } from '../core/services/firestore.service';
import { MaterialModule } from '../material-module';

const routes: Routes = [
  { path: 'sign-in', component: SignInComponent, canActivate: [SecureInnerPagesGuard]},
  { path: 'register-user', component: SignUpComponent, canActivate: [SecureInnerPagesGuard]},
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard] }
];

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    UiCoreModule,
    ReserveModule,
    HomeModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class PagesModule { }
